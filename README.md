# WikiThon

A randomly generated Wikipedia spelling marathon, programmed in Python. Made during Year 11 SDD 2021.

# How to use this program:
## Prerequisites:
To run this game, you will need:
	- A Windows 10 computer (ideally version 1909 or higher)
	- Python installed (ideally version 3.9.5 or higher)
	- A stable internet connection (program will close otherwise)

## Launching:
Launch this program on a Windows 10 computer with Python installed by double-clicking on the file titled "index.py". A command prompt window will open up with the game running inside it.

Please note that if this game is launched any other way, certain functionality including window resizing and text colour may be unavailable or broken.

## Main Menu:
After launching the game, you will be presented with a menu and four options. Here, you can opt to select your difficulty, with each level increasing the number of word choices you are presented with.

You can select an option by either typing in the number in front of it, or the first word before the = sign. Hit enter once you have typed in your choice.

After selecting your difficulty, you will be allowed to choose how many questions you are tested on. You may input a number from 1 to 20.

## After Game Start:
Once the game has finished loading, you will be presented with a random article title and extract from Wikipedia. The missing word is marked with a red underline, with the choices for that particular spot placed under it. The number of words that appear depends on the choice you made at the start of the main menu.

To enter a solution, type the word you believe is correct into the input field. The word must be capitalised and spelt correctly, no exceptions are allowed.

If you type the correct word, you will be given a point. No points are awarded for an incorrect word, and words spelt incorrectly or not capitalised properly will be ignored. You will then move on to the next scenario, if any remain.

Once all questions have been passed, you will be shown an accuracy rank percentage, and told how well you have performed. Pressing ENTER will return you to the main menu, where you may start a new game or quit.

## Closing the game:
After you have finished playing, you may quit the game by typing "9" or "quit" into the main menu. Alternatively, to force close the game you can press CTRL+C at any time, or simply close the window.

# Text To ASCII Art Generator (TAAG)
splash.txt file generated using https://patorjk.com/software/taag/#p=testall&f=Doom&t=WikiThon