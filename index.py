# Make requests to Wikipedia API.
import urllib.request as rs
# Convert response to JSON dictionary.
import json
# Randomly select and shuffle user choices.
import random
# Determine Windows version to check for color support.
import platform
# Execute CMD commands for clearing and resizing screen.
import os
# Check if user is or isn't running IDLE.
import sys
# Punctuation list for filtering word choices.
from string import punctuation
# Creating pauses / delays with time.sleep()
import time

# Color support is disabled by default.
colors = False

# Check if user is running Windows 10.
if platform.platform().startswith('Windows-10'):
    # Check if user is running version 1909 (build 18363) or higher.
    if int(platform.version().split('.')[2]) >= 18363:
        # Check if user is running IDLE or the requied CMD.
        try:
            # User is running IDLE.
            sys.stdout.shell
            print('NOTE: Colors disabled. Please double-click this file instead of opening it in IDLE.')
        except AttributeError:
            # User is not running IDLE.
            colors = True
            # Resize the window if it is too small. At least 120 columns and 50 lines.
            size = os.get_terminal_size()
            if size.columns < 120:
                os.system('mode con cols=120')
            if size.lines < 40:
                os.system('mode con lines=40')
# Otherwise, leave color support disabled as it will not work.

def colorText(text, color):
    # ANSI terminal color dictionary for use in Windows and Linux
    # i think these work on macos. i have no way to test. -M
    # Windows-specific resource --- https://ss64.com/nt/syntax-ansi.html
    # General resource          --- https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
    # Finish ANSI Sequence = "\033[0m"
    ANSI = {
        "RED": "31",
        "GREEN": "32",
        "YELLOW": "33",
        "BLUE": "34",
        "MAGENTA": "35",
        "CYAN": "36",
        "BBLACK": "90",
        "BRED": "91",
        "BGREEN": "92",
        "BYELLOW": "93",
        "BBLUE": "94",
        "BMAGENTA": "95",
        "BCYAN": "96",
    }
    
    # Check if program has verified colors for use yet.
    if colors:
        # Edge case for underline and bold. (Not included in ANSI dict. since they may be randomly selected.)
        # Underline = "\033[4m"
        # Bold = "\033[1m"
        if color == 'Underline':
            return(f'\033[4m{text}\033[0m')
        elif color == 'Bold':
            return(f'\033[1m{text}\033[0m')
        # Helper for random colors.
        elif color == 'Random':
            return f'\033[{ANSI[random.choice(list(ANSI))]}m{text}\033[0m'
        else:
            return(f'\033[{ANSI[color]}m{text}\033[0m')
    # Else just ignore colors.
    else:
        return(text)


# Function to sanitise randomly chosen words.
def sanitiseWords(words):
    # Temporarily store cleaned words.
    clean_words = []
    
    for word in words:
        # If word is longer than 3 letters, and is does not contain any punctuation,
        # add it to the clean_words array.
        if not len(word) <= 3 and not any(p in word for p in punctuation):
            clean_words.append(word)

    # Return the sanitised array.
    return clean_words

def getArticle(difficulty):
    print(colorText('Loading...', 'YELLOW'))

    '''
    Documentation for MediaWiki API at: https://www.mediawiki.org/wiki/API:Main_page
    
    RANDOM (https://www.mediawiki.org/wiki/API:Random) ---
        &generator=random : pick random page(s)
        &grnnamespace=0   : pick items only from the "Main" namespace of Wikipedia containing articles.
        &grnlimit=2       : specify two pages for the random generator.
    TEXTEXTRACTS (https://www.mediawiki.org/wiki/Extension:TextExtracts) ---
        &prop=extracts    : use TextExtracts for gathering the sentences.
        &exintro=         : return only content before the first section. boolean, set to true.
        &exchars=300      : request at least 300 characters from each extract.
        &explaintext=     : return plaintext sentences, not HTML. boolean, set to true.
    OTHER ---
        &redirects=       : automatically follow any page redirects. boolean, set to true
        &formatversion=2  : latest format version.
        &format=json      : return JSON object, not HTML or plaintext.
    '''
    # Get two random pages with titles and two extracts each from Wikipedia using MediaWiki API.
    try:
        internet_response = rs.urlopen('https://en.wikipedia.org/w/api.php?action=query&generator=random&grnnamespace=0&grnlimit=2&prop=extracts&exintro=&exchars=300&explaintext=&redirects=&formatversion=2&format=json')
    except:
        input(colorText('Error while downloading data. Press ENTER to quit, and check your internet connection.', 'BRED'))
        exit()
    # Parse content into JSON object here.
    json_content = internet_response.read()
    mediawiki = json.loads(json_content)
    
    # Narrow down to only the page title and extracts, plus suffix the printed sentence with "..."
    title1 = mediawiki['query']['pages'][0]['title']
    extract1 = mediawiki['query']['pages'][0]['extract'] + '...'
    extract2 = mediawiki['query']['pages'][1]['extract']
    
    # Check if both extracts are fully in English by determining whether they can be encoded in ASCII or not.
    # This ensures the extracts are always printed to CMD with no missing characters
    # and that the user can enter their words in English characters only.
    if not extract1.isascii() or not extract2.isascii():
        return getArticle(difficulty)

    # Clean up the real words, and the fake words.
    # Remove punctuation and short words.
    words = sanitiseWords(extract1.split())
    bogus_words = sanitiseWords(extract2.split())

    # Check if at least [DIFFICULTY] words can be presented to the user.
    if len(words) <= difficulty or len(bogus_words) <= difficulty:
        return getArticle(difficulty)

    # If no issues, return the first page title + extract, and the chosen word with all the bad words.
    chosen_word = random.choice(words)
    return [title1, extract1, chosen_word, bogus_words]
    

def runGame(difficulty, questions):
    print(f'Running a new game with {difficulty} choices and {questions} questions...\n')
    score = 0
    answered_questions = questions
    
    while answered_questions > 0:
        answered_questions -= 1
        print(colorText('Question ' + str((questions - answered_questions)), 'BCYAN'))

        # Get two articles, ensuring that their word count satisfies the number of choices the user has selected.
        article = getArticle(difficulty)
        # Organise data provided by the getArticle function.
        # Title1 and Extract1 are the sentence displayed to the user.
        # Extract2 is not displayed, and is used for fake word choices.
        title1 = article[0]
        extract1 = article[1]
        chosen_word = article[2]
        bogus_words = article[3]
        # Create a list of all words presented to user, including the correct one.
        choices = []
        # Add the correct word to the choices.
        choices.append(chosen_word)
        
        # Choose random bogus words from the OTHER sentence not shown.
        # Subtract one to compensate for the correct word.
        for x in range(difficulty - 1):
            # Submit both the whole words array, and the choices array for duplicate checking.
            temp_word = random.choice(bogus_words)
            # Remove all instances of this word so it can't be selected twice.
            while temp_word in bogus_words:
                bogus_words.remove(temp_word)
            # Add the final bogus word to the user selections.
            choices.append(temp_word)

        # Remember to shuffle the array once all words are plugged in!
        random.shuffle(choices)

        # Blank out the chosen word with underscores.
        underscore_line = colorText('_' * len(chosen_word), 'RED')
        # Print article title and sentence, with its selected word hidden by the pattern above.
        print(colorText(title1, 'Underline'))
        print(colorText(extract1.replace(chosen_word, underscore_line), 'Bold'))

        print(colorText('\nWhich word is missing? Please type the word in full.', 'BMAGENTA'))
        
        # Print every word choice in a numbered list.
        for number, choice in enumerate(choices):
            # Add 1 to number, otherwise it starts from zero.
            print(colorText(f'{number + 1}. "{choice}"', 'Random'))

        # Constantly keep requesting answers until the user provides a feasible answer.
        while True:
            answer = input(' >> ')

            if answer not in choices:
                print(colorText('I do not recognise where your answer comes from. Have you spelt and capitalised it correctly?', 'BRED'))
                time.sleep(2)
            # Correct answer
            elif answer == chosen_word:
                print(colorText('Brilliant work! +1 point for you.', 'BCYAN'))
                time.sleep(3)
                score += 1
                break
            # Incorrect answer
            else:
                print(colorText('Nice try, but the word was: ', 'BYELLOW') + colorText(chosen_word, 'BRED'))
                time.sleep(3)
                break

        os.system('cls')

    # Calculate and present score.
    result = round(score / questions, 4) * 100
    if result > 75:
        print(colorText(f'Wonderful effort! Your score was {result}%.', 'BGREEN'))
    elif result > 50:
        print(colorText(f'Valiant effort! Your score was {result}%.', 'BMAGENTA'))
    elif result > 25:
        print(colorText(f'Better luck next time. Your score was {result}%.', 'BYELLOW'))
    else:
        print(colorText(f'You were trying, right? Your score was {result}%.', 'RED'))
    
    # Return to menu
    print(colorText('Press ENTER to return to menu.', 'BCYAN'))
    input(' >> ')
    return main()

def main():
    # Clear screen when running in CMD
    os.system('cls')
    
    # Print splash.txt in a random color.
    print(colorText(open('splash.txt', 'r').read(), 'Random'))
    # Store user responses while they're being asked.
    difficulty = 0
    questions = 0

    print(colorText('Please select your difficulty:', 'Underline'))
    print(colorText('1. Easy = 3 choices', 'BGREEN'))
    print(colorText('2. Moderate = 5 choices', 'BYELLOW'))
    print(colorText('3. Challenging = 7 choices', 'BRED'))
    print(colorText('9. Quit = Close the game now and return to desktop.', 'BBLACK'))
    # Difficulty selection. Stores number of choices to be presented.
    while True:
        answer = input(" >> ")
        # if only i had switch statements. -M
        if answer.lower() == 'easy' or answer == '1':
            print(colorText('Selected EASY difficulty.', 'BGREEN'))
            difficulty = 3
            break
        elif answer.lower() == 'moderate' or answer == '2':
            print(colorText('Selected MODERATE difficulty.', 'BYELLOW'))
            difficulty = 5
            break
        elif answer.lower() == 'challenging' or answer == '3':
            print(colorText('Selected CHALLENGING difficulty. Good luck!', 'BRED'))
            difficulty = 7
            break
        elif answer.lower() == 'quit' or answer == '9':
            print(colorText('Quitting game now...', 'BBLACK'))
            time.sleep(1)
            exit(0)
        else:
            print(colorText('I did not recognise your answer, please try again?', 'BRED'))
            time.sleep(2)

    time.sleep(1)
    print(colorText('How many questions would you like to solve? (1 - 20)', 'Underline'))
    # Number of questions selection. Check that it's between 1 and 20.
    while True:
        try:
            answer = int(input(' >> '))

            if answer < 1 or answer > 20:
                print(colorText(f'We can\'t ask you {str(answer)} questions! Please try again.', 'BRED'))
                time.sleep(2)
            else:
                questions = answer
                break
        # Use except here, since isDigit would count "²" as a number.
        # Also only select ValueError's, so you can still Ctrl+C to quit.
        except ValueError:
            print(colorText('Please enter only numbers.', 'BRED'))

    os.system('cls')
    runGame(difficulty, questions)

main()
